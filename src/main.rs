use std::{
    process::Command,
    env,
    io,
    io::prelude::*,
    process
};

fn pause() {
    let mut stdin = io::stdin();
    let mut stdout = io::stdout();
    write!(stdout, "Press any key to continue...").unwrap();
    stdout.flush().unwrap();
    let _ = stdin.read(&mut [0u8]).unwrap();
}

fn main() {
    let exe = env::current_exe().unwrap();
    let curdir = exe.parent().unwrap();
    let mut iwpo = Command::new(curdir.join("data/node-portable.exe"));
    let mut args = vec![
        String::from("--max_old_space_size=8192"),
        String::from("index.js"),
    ];
    let mut remove = true;
    for argument in env::args() {
        if remove {
            remove = false;
            continue;
        }
        args.push(String::from(argument));
    }
    iwpo.args(&args);
    iwpo.current_dir(curdir.join("data"));
    iwpo.status().unwrap_or_else(|e| {
        println!("{}", e);
        pause();
        process::exit(1);
    });
}
