extern crate winres;

fn main() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("./src/icon.ico");
    res.compile().unwrap();
}
